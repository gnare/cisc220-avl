/*
 * TNode.cpp
 *
 *  Created on: Apr 3, 2020
 *      Author: 13027
 */

#include <iostream>
#include <string>
#include "TNode.hpp"
using namespace std;

TNode::TNode(string sarr[]) {
	left = NULL;
	right = NULL;
	parent = NULL;
	height = 1;
//	cout << "in constructor: "<<endl;
//	for (int i = 0; i < 5; i++) {
//		cout << sarr[i] << endl;
//	}
	data = new StudInf(sarr);//updated
}

TNode::TNode(StudInf* s) {
	left = NULL;
	right = NULL;
	parent = NULL;
	height = 1;
	data = s;
}

TNode::TNode() {
	left = NULL;
	right = NULL;
	parent = NULL;
	height = 1;
	string sarr[] = {"Adams","chocolate", "Alticia","I tried to decapitate my brother","fried tarantulas", "toothbrush that acidifies teeth"};
	data = new StudInf(sarr); // updated
}

TNode::~TNode(){
	cout <<"Deleting "<<data->first<<","<<data->last<<endl;
}

void TNode::printNode() {
	cout << "Height of node: " << height << endl;
	cout << "Name: ";
	cout << data->first << " "<<data->last << endl;
	cout << "Weirdest thing you ate: " << data->ate << endl;
	cout << "Unique thing: " << data->strange << endl;
	cout << "Favorite Dessert: " << data->dessert << endl;
	cout << "Invention: " << data->invent << endl;
	cout << "*************************************************************"<<endl;
}
